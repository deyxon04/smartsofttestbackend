import { Module } from '@nestjs/common';

import { TableDataService } from './services/tabledata.service';
import { TableData1 } from 'src/shared/entity/tableData1.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TableDataController } from './controllers/tabledata.controller';
import { TableData2 } from 'src/shared/entity/tableData2.entity';
import { TableData3 } from 'src/shared/entity/tableData3.entity';
@Module({
    imports: [
        TypeOrmModule.forFeature([TableData1, TableData2, TableData3])
    ],
    controllers: [TableDataController],
    providers: [TableDataService],
})
export class DataModule { }
