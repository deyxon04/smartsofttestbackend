import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { retry } from 'rxjs/operators';
import { TableData1 } from 'src/shared/entity/tableData1.entity';
import { TableData2 } from 'src/shared/entity/tableData2.entity';
import { TableData3 } from 'src/shared/entity/tableData3.entity';
import { Repository } from 'typeorm';
@Injectable()
export class TableDataService {



    constructor(
        @InjectRepository(TableData1) private tabledata1: Repository<TableData1>,
        @InjectRepository(TableData2) private tabledata2: Repository<TableData2>,
        @InjectRepository(TableData3) private tabledata3: Repository<TableData3>,

    ) { }



    async getAll(tableTypeId) {
        switch (tableTypeId) {
            case 1:
                return await this.tabledata1.find()
            case 2:
                return await this.tabledata2.find()

            case 3:
                return await this.tabledata3.find()
        }
    }

    async insert(tableData) {
        let data = null
        switch (tableData['TableTypeId']) {
            case 1:
                data = this.tabledata1.create(tableData)
                await this.tabledata1.insert(data)
                return data
            case 2:
                data = this.tabledata2.create(tableData)
                await this.tabledata2.insert(data)
                return data
            case 3:
                data = this.tabledata3.create(tableData)
                await this.tabledata3.insert(data)
                return data
        }
    }

    async delete(id, tableTypeId) {
        switch (tableTypeId) {
            case 1:
                await this.tabledata1.delete(id)
                break;
            case 2:
                await this.tabledata2.delete(id)
                break;
            case 3:
                await this.tabledata3.delete(id)
                break;
        }
    }


    async update(id, tableTypeId, data) {
        switch (tableTypeId) {
            case 1:
                await this.tabledata1.update({ id }, data)
                return await this.tabledata1.findOne({ id });
            case 2:
                await this.tabledata2.update({ id }, data)
                return await this.tabledata2.findOne({ id });
            case 3:
                await this.tabledata3.update({ id }, data)
                return await this.tabledata3.findOne({ id });
        }
    }



    findById() { }


    findAll() { }




}
