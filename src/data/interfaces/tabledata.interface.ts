export interface TableData {

    T1C1: number
    T1C2: string
    T1C3: number
    T1C4: Date
    TableTypeId?: number
}