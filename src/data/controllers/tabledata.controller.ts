import { Body, Controller, Get, HttpStatus, Post, Put, Res, Delete, Param } from '@nestjs/common';
import { TableDataService } from '../services/tabledata.service';
@Controller('tableData')

export class TableDataController {



    constructor(private tableDataService: TableDataService) { }




    @Get(':id')
    async GetAll(@Res() response, @Param() { id }) {

        console.log(id);

        this.tableDataService.getAll(parseInt(id)).then(tableInfo => {
            return response.status(HttpStatus.OK).json(tableInfo)
        }).catch(err => {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
        })
    }



    @Post()
    async insertHandler(@Res() response, @Body() body) {
        this.tableDataService.insert(body).then(table => {
            return response.status(HttpStatus.OK).json(table)
        }).catch(err => {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
        })
    }


    @Put(':id/:tableTypeId')
    async updateHandler(@Res() response, @Body() body, @Param() { id, tableTypeId }) {
        this.tableDataService.update(id, parseInt(tableTypeId), body).then(item => {
            return response.status(HttpStatus.OK).json(item)
        }).catch(err => {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
        })
    }

    @Delete(':id/:tableTypeId')
    async deleteHandler(@Res() response, @Param() { id, tableTypeId }) {
        this.tableDataService.delete(id, parseInt(tableTypeId)).then(tables => {
            return response.status(HttpStatus.OK).json({ message: 'Item eliminado' })
        }).catch(err => {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
        })
    }


    @Get(':id')
    async GetOne(@Res() response, @Body() body) {
        this.tableDataService.insert(body).then(tables => {
            return response.status(HttpStatus.OK).json(tables)
        }).catch(err => {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
        })
    }



}
