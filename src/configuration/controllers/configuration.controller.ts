import { Controller, Get, HttpStatus, Param, Res } from '@nestjs/common';
import { ConfigurationService } from '../services/configuration.service';

@Controller('configuration')
export class ConfigurationController {



    constructor(private configurationService: ConfigurationService) { }

    @Get('getTables')
    async getTables(@Res() response) {
        this.configurationService.getTables().then(tables => {
            return response.status(HttpStatus.OK).json(tables)
        }).catch(err => {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
        })
    }


    @Get('getTableDetail/:id')
    async getTableDetail(@Res() response, @Param() { id }) {
        this.configurationService.getTableDetail(id).then(tableDetail => {
            response.status(HttpStatus.OK).json(tableDetail)
        }).catch(err => {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
        })
    }
}






