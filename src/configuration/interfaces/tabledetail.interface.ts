
export interface TableDetail {
    id: number;
    name: string
    colums: Colums
}


interface Colums {
    id: number,
    header: string,
    dataType: string,
    required: false
    format?: string,
}