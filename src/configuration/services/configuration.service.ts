import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TableStructure } from 'src/shared/entity/tableStructure.entity';
import { Repository } from 'typeorm';
import { TableType } from '../../shared/entity/tableType.entity';

@Injectable()
export class ConfigurationService {



    constructor(
        @InjectRepository(TableType) private tableTypeRepository: Repository<TableType>,
        @InjectRepository(TableType) private tableStructureRepository: Repository<TableStructure>
    ) { }



    async getTables(): Promise<TableType[]> {
        return await this.tableTypeRepository.find()
    }

    async getTableDetail(id: number) {
        let tableDetail = await this.tableTypeRepository.findOne({ where: { id } })
        tableDetail['colums'] = await this.tableStructureRepository.query(`SELECT id, Header as header, dataType, format, required FROM tablestructure WHERE TableTypeId = ${id}`)
        return tableDetail
    }

}
