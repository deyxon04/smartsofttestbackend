import { ConfigurationService } from './services/configuration.service';
import { ConfigurationController } from './controllers/configuration.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TableType } from '../shared/entity/tableType.entity';
import { TableStructure } from 'src/shared/entity/tableStructure.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([TableType, TableStructure])
    ],
    controllers: [ConfigurationController],
    providers: [ConfigurationService],
})
export class ConfigurationModule { }
