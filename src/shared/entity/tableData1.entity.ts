import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';


@Entity({ name: 'TableData1' })
export class TableData1 {
    @PrimaryGeneratedColumn('increment') id: number;
    @Column({ type: 'int', nullable: false }) T1C1: string;
    @Column({ length: 50, type: 'varchar', nullable: false }) T1C2: string;
    @Column({ type: 'int', nullable: true }) T1C3: string;
    @Column({ type: 'datetime', nullable: true }) T1C4: string;

}
