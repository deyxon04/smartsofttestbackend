import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';


@Entity({ name: 'TableData3' })
export class TableData3 {

    @PrimaryGeneratedColumn('increment') id: number;
    @Column({ type: 'int', nullable: false }) T3C1: string;
    @Column({ length: 50, type: 'varchar', nullable: false }) T3C2: string;
    @Column({ type: 'datetime', nullable: false }) T3C4: string;
}
