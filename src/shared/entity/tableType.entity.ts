import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'TableType' })
export class TableType {
    @PrimaryGeneratedColumn('increment') id: number;
    @Column({ length: 30, type: 'varchar', nullable: false }) name: string;
}
