import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';


@Entity({ name: 'TableData2' })
export class TableData2 {
    @PrimaryGeneratedColumn('increment') id: number;
    @Column({ type: 'int', nullable: false }) T2C1: string;
    @Column({ length: 50, type: 'varchar', nullable: true }) T2C2: string;
    @Column({ type: 'int', nullable: true }) T2C3: string;
    @Column({ type: 'datetime', nullable: false }) T2C4: string;
    @Column({ type: 'int', nullable: false }) T2C5: string;

}
