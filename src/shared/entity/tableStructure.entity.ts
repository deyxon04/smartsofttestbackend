import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    RelationId,
} from 'typeorm';

import { TableType } from './tableType.entity';

@Entity({ name: 'TableStructure' })
export class TableStructure {
    @PrimaryGeneratedColumn('increment') id: number;
    @ManyToOne(type => TableType)
    @JoinColumn({ name: 'TableTypeId', referencedColumnName: 'id' }) TableTypeId: number
    @Column({ length: 20, type: 'varchar', nullable: false }) Header: string;
    @Column({ length: 10, type: 'varchar', nullable: false }) dataType: string;
    @Column({ length: 20, type: 'varchar', nullable: true }) format: string;
    @Column({ type: 'boolean', nullable: false }) required: string;
}
