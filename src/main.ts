import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as cors from 'cors'
import { bold } from "chalk";
async function bootstrap() {
  const port = process.env.PORT || 8080;
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.use(cors())
  app.setGlobalPrefix('api');

  await app.listen(port);
  Logger.log(
    bold.blue(process.env.APPLICATION_NAME) +
    bold.yellow(' RUNNING ON ') +
    bold.green(`http://localhost:${port}`),
    'Bootstrap',
  );
}

bootstrap();
