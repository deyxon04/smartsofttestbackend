
import { DataModule } from './data/data.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './shared/utils/logging.interceptor';
import { HttpErrorFilter } from './shared/utils/http.error.filter';
@Module({
  imports: [
    DataModule,
    ConfigurationModule,
    TypeOrmModule.forRoot()
  ],
  providers: [{
    provide: APP_INTERCEPTOR,
    useClass: LoggingInterceptor
  },
  {
    provide: APP_FILTER,
    useClass: HttpErrorFilter
  }],
})
export class AppModule { }
